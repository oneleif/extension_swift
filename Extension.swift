//
//  Extension.swift
//
//  Created by Zach Eriksen on 1/4/16.
//  Copyright © 2016 Zach Eriksen. All rights reserved.
//

import Foundation
import UIKit

let time_hour : () -> Double = {return Double(NSCalendar.currentCalendar().components(.Hour, fromDate: NSDate()).hour)}
let time_minute : () -> Double = {return Double(NSCalendar.currentCalendar().components(.Minute, fromDate: NSDate()).minute)}
let time_second : () -> Double = {return Double(NSCalendar.currentCalendar().components(.Second, fromDate: NSDate()).second)}
let screen_height = UIScreen.mainScreen().bounds.height
let screen_width = UIScreen.mainScreen().bounds.width

func askForNotificationPermission (){
    let notificationSettings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
    UIApplication.sharedApplication().registerUserNotificationSettings(notificationSettings)
}

func createUILocalNotification(seconds : Double, title: String, body : String) {
    let notification = UILocalNotification()
    notification.fireDate = NSDate(timeIntervalSinceNow: seconds)
    notification.alertTitle = title
    notification.alertBody = body
    notification.soundName = UILocalNotificationDefaultSoundName
    UIApplication.sharedApplication().scheduleLocalNotification(notification)
}

extension UIBezierPath {
    static func makeCircle(center : CGPoint, radius : CGFloat, color : UIColor) -> CAShapeLayer {
        let circlePath = UIBezierPath(arcCenter: center, radius: radius, startAngle: CGFloat(-M_PI_2), endAngle:CGFloat(M_PI + M_PI_2), clockwise: true)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.CGPath
        
        //change the fill color
        shapeLayer.fillColor = UIColor.clearColor().CGColor
        //you can change the stroke color
        shapeLayer.strokeColor = color.CGColor
        shapeLayer.strokeEnd = 0.0
        //you can change the line width
        shapeLayer.lineWidth = 40.0
        
        return shapeLayer
    }
}

extension Int {
    func times(task: () -> ()) {
        for _ in 1...self {
            task()
        }
    }
    
    var isEven: Bool { return self % 2 == 0 }
    
    var isOdd: Bool { return !isEven }
    
    var isPositive: Bool { return self > 0 }
    
    var isNegative: Bool { return self < 0 }
    
    var toDouble: Double { return Double(self) }
    
    var toFloat: Float { return Float(self) }
    
    var digits: Int {
        if self == 0 {
            return 1
        }
        else if(Int(fabs(toDouble)) <= LONG_MAX) {
            return Int(log10(fabs(toDouble))) + 1
        }
        else {
            return -1;
        }
    }
    
    var isPrime: Bool {
        if self < 2  { return false }
        return (2..<self).filter { self % $0 == 0 }.count == 0
    }
}

extension Double {
    func round(decimals: Int) -> Double {
        let format : NSNumberFormatter = NSNumberFormatter()
        format.numberStyle = NSNumberFormatterStyle.DecimalStyle
        format.roundingMode = NSNumberFormatterRoundingMode.RoundHalfUp
        format.maximumFractionDigits = decimals
        let string: NSString = format.stringFromNumber(NSNumber(double: self))!
        return string.doubleValue
    }
}

extension String {
    var length : Int { return self.characters.count }
    var int : Int { return Int(self)!}
    subscript (i: Int) -> Character {
        return self[self.startIndex.advancedBy(i)]
    }
    
    func reverse() -> String {
        return (1...length)
            .map { "\(self[length - $0])" }
            .joinWithSeparator("")
    }
    
    func toCharArray() -> [Character] {
        var array : [Character] = []
        for i in 0 ..< self.length {
            array.append(self[i])
        }
        return array
    }
}

extension UIColor {
    static func randomColor() -> UIColor {
        func randomCGFloat() -> CGFloat {
            return CGFloat(arc4random()) / CGFloat(UInt32.max)
        }
        let r = randomCGFloat()
        let g = randomCGFloat()
        let b = randomCGFloat()
        return UIColor(red: r, green: g, blue: b, alpha: 1.0)
    }
    
    static func rgba(r : CGFloat, _ g : CGFloat, _ b :CGFloat, _ a : CGFloat) -> UIColor {
        return UIColor(red: r/255, green: g/255, blue: b/255, alpha: a)
    }
    
    convenience init(hexString:String) {
        let hexString:NSString = hexString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let scanner            = NSScanner(string: hexString as String)
        
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        
        var color:UInt32 = 0
        scanner.scanHexInt(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        
        self.init(red:red, green:green, blue:blue, alpha:1)
    }
    
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        return NSString(format:"#%06x", rgb) as String
    }
}
